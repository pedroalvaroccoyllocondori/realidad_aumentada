AFRAME.registerComponent("listen-to-scale",{
    init:function(){
        this.scaleFactor=0.15

        this.handleClick=this.handleClick.bind(this)
        var grande=document.getElementById("grande")
        var pequeno=document.getElementById("pequeno")

        grande.addEventListener("click",this.handleClick)
        pequeno.addEventListener("click",this.handleClick)
    },
    handleClick: function(e){
       // console.log(e.target.id)
       if (e.target.id==="grande") {
           this.scaleFactor*=1.5
       }else{
          this.scaleFactor*=0.6
       }
       this.el.object3D.scale.x =this.scaleFactor
       this.el.object3D.scale.y =this.scaleFactor
       this.el.object3D.scale.z =this.scaleFactor

    }


})